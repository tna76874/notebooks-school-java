FROM jupyter/scipy-notebook:python-3.8.13

USER root

ENV SETUP_STATUS="production"
ENV REPO_USER="tna76874"
ENV REPO_NAME="notebooks-school"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -q && apt-get install -y \
    ghostscript \
    software-properties-common \
    python3-tk
RUN add-apt-repository universe --yes --update && apt-get install -y \
    shc \
    gcc &\
    rm -rf /var/lib/apt/lists/*

COPY ./scripts/docker-entrypoint.sh /
COPY ./scripts/run_on_init /usr/local/bin/
COPY ./scripts/update_notebooks /usr/local/bin/
RUN chmod 775 /usr/local/bin/run_on_init
RUN chmod 775 /usr/local/bin/update_notebooks


ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN chmod 775 /docker-entrypoint.sh

COPY ./java /java
RUN mkdir /work
RUN chown -R ${NB_USER} /java && chown -R ${NB_USER} /work && chmod 775 -R /java

USER ${NB_USER}

COPY ./requirements.txt . 

RUN python3 -m pip install --no-cache-dir notebook jupyterlab jupyterhub &&\
    pip install --no-cache-dir -r requirements.txt &&\
    pip install jupyter_contrib_nbextensions ipywidgets &&\
    jupyter contrib nbextension install --user &&\
    jupyter nbextension enable varInspector/main && \
    rm -rf requirements.txt

USER root

RUN cd /java && python install.py --replace

RUN apt-get update -y && \
    apt-get install -y openjdk-11-jdk

USER ${NB_USER}

RUN rm -rf ${HOME}/work && mkdir -p ${HOME}/work

ENTRYPOINT ["/docker-entrypoint.sh"]